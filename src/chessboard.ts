import { Piece, getPiece } from './piece'

export class Chessboard {
  fen: string
  spaces: Array<null | Piece>

  constructor(fen: string) {
    this.fen = fen
    this.spaces = new Array(64).fill(null)

    this.parseFen(this.fen)
  }

  set(fen: string) {
    this.parseFen(fen)
    this.fen = fen
  }

  private parseFen(fen: string) {
    const rows: string[] = fen.split('/')
    let space: number = this.spaces.length

    rows.forEach(row => {
      space -= 8

      let index: number = space

      Array.from(row).forEach((entry: string) => {
        if (!isNaN(parseInt(entry))) {
          index += parseInt(entry)
          return
        }

        this.spaces[index] = getPiece(entry)
        ++index
      })
    })
  }

  public generateFen(): string {
    let fen: string[] = []
    let emptySquareCount: number = 0

    for (let rank = 7; rank >= 0; --rank) {
      for (let file = 0; file < 8; ++file) {
        const space: number = rank * 8 + file
        const piece: Piece = this.spaces[space]

        if (piece) {
          if (emptySquareCount > 0) {
            fen.push(emptySquareCount.toString())
            emptySquareCount = 0
          }
          fen.push(piece.fen)
        } else {
          ++emptySquareCount
        }

        if ((space + 1) % 8 === 0 && space < 64) {
          if (emptySquareCount > 0) {
            fen.push(emptySquareCount.toString())
            emptySquareCount = 0
          }

          if (rank > 0) fen.push('/')
        }
      }
    }

    /**
     * fen += c.activeColor === 'w' ? ' b' : ' w'
     * fen += ' ' + c.availableCastle
     * fen += ' ' + c.enpassantTarget
     * fen += ' ' + c.halfmoveClock
     * fen += ' ' + c.fullmoveNumber
    */

    return fen.join('')
  }
}
