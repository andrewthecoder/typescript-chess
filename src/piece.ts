export type Piece = {
  fen: string
  type: 'Pawn' | 'Rook' | 'Knight' | 'Bishop' | 'Queen' | 'King'
  color: 'white' | 'black'
  glyph: string
} | null

export const getPiece = (fen: string): Piece | null => {
  const mapping: Record<string, Piece> = {
    p: { fen: 'p', type: 'Pawn', color: 'black', glyph: '♟' },
    r: { fen: 'r', type: 'Rook', color: 'black', glyph: '♜' },
    n: { fen: 'n', type: 'Knight', color: 'black', glyph: '♞' },
    b: { fen: 'b', type: 'Bishop', color: 'black', glyph: '♝' },
    q: { fen: 'q', type: 'Queen', color: 'black', glyph: '♛' },
    k: { fen: 'k', type: 'King', color: 'black', glyph: '♚' },
    P: { fen: 'P', type: 'Pawn', color: 'white', glyph: '♟' },
    R: { fen: 'R', type: 'Rook', color: 'white', glyph: '♜' },
    N: { fen: 'N', type: 'Knight', color: 'white', glyph: '♞' },
    B: { fen: 'B', type: 'Bishop', color: 'white', glyph: '♝' },
    Q: { fen: 'Q', type: 'Queen', color: 'white', glyph: '♛' },
    K: { fen: 'K', type: 'King', color: 'white', glyph: '♚' }
  }

  return mapping[fen] || null
}

