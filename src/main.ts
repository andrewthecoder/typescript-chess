import '../src/style.css'
import { Piece } from './piece'
import { Chessboard } from './chessboard'

const canvas = document.getElementById('chessboard') as HTMLCanvasElement
const ctx = canvas.getContext('2d') as CanvasRenderingContext2D
const squareSize: number = canvas.width / 8

const black: string = '#716777'
const white: string = '#cdb6ae'

const chessboard: Chessboard = new Chessboard('rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR')

const drawGame = () => {
  drawChessboard()
  arrangeChessboard()
}

const drawChessboard = () => {
  for (let rank = 0; rank < 8; ++rank) {
    for (let file = 0; file < 8; ++file) {
      const x: number = file * squareSize
      const y: number = rank * squareSize
      const color: string = (rank + file) % 2 === 0 ? white : black

      ctx.fillStyle = color
      ctx.fillRect(x, y, squareSize, squareSize)
    }
  }
}

const arrangeChessboard = () => {
  for (let rank = 7; rank >= 0; --rank) {
    for (let file = 0; file < 8; ++file) {
      const piece: Piece = chessboard.spaces[rank * 8 + file]

      if (piece) {
        const x = file * squareSize + squareSize / 2
        const y = (7 - rank) * squareSize + squareSize / 2

        ctx.font = `${squareSize * 0.6}px Arial`
        ctx.textAlign = 'center'
        ctx.textBaseline = 'middle'
        ctx.fillStyle = piece.fen === piece.fen.toUpperCase() ? 'white' : 'black'
        ctx.fillText(piece.glyph, x, y)
      }
    }
  }
}

/** DRAG AND DROP FUNCTIONS */
let origin: number = 0
let selectedPiece: Piece = null

const pickup = (event: MouseEvent) => {
  const x: number = event.offsetX
  const y: number = event.offsetY

  const file: number = Math.floor(x / squareSize)
  const rank: number = 7 - Math.floor(y / squareSize)
  const space: number = rank * 8 + file

  origin = space

  const piece: Piece = chessboard.spaces[space]

  if (piece) {
    canvas.classList.add('grabbing')

    selectedPiece = piece

    drawGame()
  }
}

const drag = (event: MouseEvent) => {
  if (selectedPiece) {
    const rect = canvas.getBoundingClientRect()
    const x: number = event.clientX - rect.left
    const y: number = event.clientY - rect.top

    chessboard.spaces[origin] = null

    drawGame()

    const file: number = Math.floor(x / squareSize)
    const rank: number = Math.floor(y / squareSize)

    ctx.font = `${squareSize * 0.6}px Arial`
    ctx.textAlign = 'center'
    ctx.textBaseline = 'middle'
    ctx.fillStyle = selectedPiece.color
    ctx.fillText(selectedPiece.glyph, (file + 0.5) * squareSize, (rank + 0.5) * squareSize)
  }
}

const drop = (event: MouseEvent) => {
  if (selectedPiece) {
    canvas.classList.remove('grabbing')

    const x: number = event.offsetX
    const y: number = event.offsetY

    const file: number = Math.floor(x / squareSize)
    const rank: number = 7 - Math.floor(y / squareSize)
    const space: number = rank * 8 + file

    /** avoid friendly fire! */
    const destination: Piece = chessboard.spaces[space]

    if (!destination || selectedPiece.color !== destination.color) {
      chessboard.spaces[space] = selectedPiece
    } else {
      chessboard.spaces[origin] = selectedPiece
    }

    selectedPiece = null
    origin = 0

    chessboard.set(chessboard.generateFen())
    console.log(chessboard.generateFen())

    drawGame()
  }
}

drawGame()

canvas.addEventListener('mousedown', pickup)
canvas.addEventListener('mousemove', drag)
canvas.addEventListener('mouseup', drop)

